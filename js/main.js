//Теория
//Я понимаю DOM, как модель, которая представляет все элементы HTML, как объекты в JS и позволяет их создавать, изменять, удалять и т.д.
//Задание
function getNewArr(arr) {
    let newArr = arr.map((el) => {
        return `<li>${el}</li>`;
    });

    let ul = document.createElement('ul');
        for (let i = 0; i < arr.length; i++) {
            ul.insertAdjacentHTML('beforeend', newArr[i]);
        }
    return document.body.append(ul);
}

let array = ['hello', 'world', 'Kiev', 'Kharkiv', 'Odessa', 'Lviv'];
getNewArr(array);
